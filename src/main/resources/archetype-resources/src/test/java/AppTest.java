package $package;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 * Represents the unit tests for the App class.
 */
public class AppTest {

    /**
     * Trivial test (always true)
     */
    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }
}