package $package;

/**
 * The App class represents the application.
 */
public class App {

    /**
     * Entry point of the program. The first instruction of the program is the
     * first instruction of the main procedure (main procedure). This
     * procedure is called by the Java interpreter.
     *
     * @param args command line parameters
     */
    public static void main(String[] args) {


        // TODO: Replace the "Hello World" program code in Java with the
        //       code of your program.

        //-----------------Java Hello World Program ----------------------
        // Declaration of the variable myString of type String
        // The declaration of a variable informs the compiler that we
        // want to use a variable by specifying a type and a name
        // for this variable.
        String myString;

        // Assigning a string to the variable myString.
        // The assignment replaces the value of the variable on the
        // left of the assignment operator (the equal sign =) with the value of
        // the expression on the right of the assignment operator.
        myString = "Hello world!";

        // Calls the System.out.println procedure passing the value of the
        // myString variable as a parameter. Calling a procedure starts
        // the execution of the procedure (subroutine) with the parameter values
        // as input data. The program continues when the execution of the
        // subroutine ends. A procedure has an effect (here, displaying a string
        // in the terminal window), but does not produce a value.
        System.out.println(myString);
        //---------------------------------------------------------------------


        // WARNING: Do not delete the two braces below.
        // The first one closes the block of the main procedure and the second
        // closes the block of the App class.
    }
}